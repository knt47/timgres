#!/usr/bin/python3
# coding: utf8

# The Incredible Machine (TIM) level/image resource reader, version 0.2
# By knt47, licensed under the Creative Commons Zero license

from __future__ import division, print_function
from timgres.cli.climain import cli_main

# Main script for CLI usage.

if __name__ == '__main__':
    cli_main()
