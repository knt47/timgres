#!/usr/bin/python3
# coding: utf8

# The Incredible Machine (TIM) level/image resource reader, version 0.2
# By knt47, licensed under the Creative Commons Zero license

from __future__ import division, print_function
from timgres.gui.guimain import gui_main

# Main script for GUI usage.

if __name__ == '__main__':
    gui_main()
